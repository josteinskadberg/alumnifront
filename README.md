# AlumniFront

Frontend repo for alumni

https://alumnifront.herokuapp.com/

## Team:
Amalie Espeseth - https://gitlab.com/amalie.e

Erlend Hollund - https://gitlab.com/Holdude

Jostein Skadberg - https://gitlab.com/josteinskadberg

Jonas Svåsand - https://gitlab.com/jsva


## Description
This is the frontend app for a project to make a web-app to host a portal for Alumni of a given entity. It allows alumni of the entity to group up and communicate through text posts on the site. The user can browse each other posts in the groups, and look at one anothers profiles. They can also make events that they invite groups to, and communicate about the events.

It is written in React with Redux.

It is hosted on Heroku, and gets data from our backend hosted on Azure (see: https://gitlab.com/jsva/alumniapi)
Request to our API requires authentication that we have solved through hosting our own auth service, using
Keycloak. 


## Instructions

### How to use:

Click log in on the home page. You can register a new account on the login screen. 

After you have logged in, you can browse groups, and join ones that you want. In these groups you can read posts, comment on them and make new posts.
On your timeline you can see an aggregate of all posts in the groups you are subscribed to. 
In the calendar you can see upcoming events that you are invited to. You can also create a new event, and invite groups to join your event. 
In a group you can also see an overview of upcoming events for that group. You can also make posts targeted to events. 

See the User Guide in the root of the repo for a full guide to the app.

### How to host your own version

You will need to host your own version of the backend to get data from. 
Follow the instructions of https://gitlab.com/jsva/alumniapi , and deploy it to the web so you can request data from it.

You will also need to host your own keycloak instance, that you can use for authentication and authorization.

**Step by step guide for own deployment:**
1. Clone the repository to your computer
2. Open the src files in a editor.
3. Fill in the appropiate links to your keycloak in `public/keycloak.json`
   - In keycloak you will need to configure a client, and provide it with the url to your frontend, so that keycoak will allow you to redirect to it and authenticate through it.
4. Replace the url to the backend with your own in line 4 of `src/services/APIService.js` 
   - This also assumes that you have hosted a complete clone of our backend and not changed any endpoints.
5. Check that the app runs with `npm start` in the alumni-front folder
6. Deploy to Heroku by following the steps here : https://blog.heroku.com/deploying-react-with-zero-configuration
    - You can skip the create-react-app part as you are not creating a new app
