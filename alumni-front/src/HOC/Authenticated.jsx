import { Navigate } from 'react-router-dom'
import AuthService from '../Services/AuthService'

const Authenticated = Component => props => {

	const isLoggedIn = AuthService.isLoggedIn()

	if (isLoggedIn) {
		return <Component {...props} />
	} else {
		return <Navigate to="/" />
	}

}
export default Authenticated
