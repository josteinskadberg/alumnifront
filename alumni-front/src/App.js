import logo from './logo.svg';
import './App.css';
import AppContainer from './HOC/AppContainer';
import Login from './Pages/Login/Login';
import Timeline from './Pages/Timeline/Timeline';
import Register from './Pages/Register/Register';
import Notfound from './Pages/NotFound/Notfound';
import Profile from './Pages/Profile/Profile';
import ProfileSettings from './Pages/Profile/ProfileSettings';
import TopicList from './Pages/Topic/TopicList';
import GroupList from "./Pages/Group/GroupList";
import GroupSingle from "./Pages/Group/GroupSingle";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navbar/Navbar';
import Calender from './components/Calender/Calender';
import GroupCalendar from './Pages/Group/GroupCalender';
import ProfileCalendar from './Pages/Profile/ProfileCalendar';
import Event from './components/Event/Event';
import CreatePostPage from './Pages/CreatePost/CreatePostPage';

import {
  BrowserRouter,
  Routes,
  Route,
  Router,
} from "react-router-dom"
import CardComponent from './components/Card/CardComponent';
import AuthService from './Services/AuthService';
import EventsList from './Pages/Event/EventsList';
import EventSingle from './Pages/Event/EventSingle';
import EventInvite from './Pages/Event/EventInvite';



function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppContainer>
          { AuthService.isLoggedIn() && <Navbar/>}
        </AppContainer>
        <Routes>
          <Route path="/" element={<Login/>}></Route>
          <Route path="register" element={<Register/>}></Route>
          <Route path="timeline" element={<Timeline/>}></Route>
          <Route path="profile" element={<Profile/>}></Route>
          <Route path="profile/settings" element={<ProfileSettings/>}></Route>
          <Route path="topics" element={<TopicList/>}></Route>
          <Route path="groups" element={<GroupList/>}></Route>
          <Route path="calender" element={<ProfileCalendar/>}></Route>
          <Route path='groups/calender' element = {<GroupCalendar/>}></Route>
          <Route path="event/:eventId" element = {<Event/>}></Route>
          <Route path="groups/groupSingle/:groupId" element={<GroupSingle/>}></Route>'
          <Route path = "/user/:username/:userId" element = {<Profile/>}></Route>
          <Route path='events' element={<EventsList/>}></Route>
          <Route path='events/eventSingle/:eventId' element={<EventSingle/>}></Route>
          <Route path="events/eventInvite/:eventId" element={<EventInvite/>}></Route>
          <Route path='createPost' element={<CreatePostPage/>}></Route>
          <Route path="*" element = {<Notfound/>}> </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
