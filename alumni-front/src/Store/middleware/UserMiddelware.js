import {UserAPI} from "../../Services/APIService"
import {
    ACTION_OTHER_USER_SET,
    ACTION_USER_EDIT,
    ACTION_USER_SET,
    userErrorAction,
    userSuccessAction,
    

} from "../actions/UserAction"

export const userMiddleware = ({ dispatch }) => (next) => async (action) => {
    next(action)

    if (action.type === ACTION_USER_SET){
    UserAPI.getUser()
            .then(user => {
                dispatch(userSuccessAction(user))
            })
            .catch(error => {
                dispatch(userErrorAction(error.message))
            })
    }

    if (action.type === ACTION_OTHER_USER_SET){
        UserAPI.getUserProfile(action.payload)
                .then(user => {
                    dispatch(userSuccessAction(user))
                })
                .catch(error => {
                    dispatch(userErrorAction(error.message))
                })
        }

    if (action.type === ACTION_USER_EDIT){
        UserAPI.editUserProfile(action.payload)
        .then(user => {
            dispatch(userSuccessAction(user))
        })
        .catch(error => {
            dispatch(userErrorAction(error.message))
        })
    }

}
    
