import { applyMiddleware } from "redux";
import { postMiddleware } from "./PostMiddleware";
import { GroupMiddleware } from "./GroupMiddleware";
import { userMiddleware } from "./UserMiddelware";
import {EventMiddleware}from "./EventMiddleware"
import { ThreadMiddleware } from "./ThreadMiddleware";

export default applyMiddleware(
    postMiddleware,
    GroupMiddleware, 
    userMiddleware,
    EventMiddleware,
    ThreadMiddleware
)