import {groupAPI} from "../../Services/APIService"
import {
    ACTION_GROUPS_SET,
    groupSuccessAction,
    groupErrorAction

} from "../actions/GroupActions"

export const GroupMiddleware = ({ dispatch }) => (next) => async (action) => {
    next(action)

    if (action.type === ACTION_GROUPS_SET)
     groupAPI.getGroups()
            .then(groups => {
                dispatch(groupSuccessAction(groups))
            })
            .catch(error => {
                dispatch(groupErrorAction(error.message))
            })
    }
    
