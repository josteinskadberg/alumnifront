import {PostApi} from "../../Services/APIService"
import {
    ACTION_POST_FETCHALL,
    fetchPostsErrorAction,
    fetchPostsSuccessAction,

} from "../actions/PostActions"

export const postMiddleware = ({ dispatch }) => (next) => async (action) => {
    next(action)

    if (action.type === ACTION_POST_FETCHALL){
    PostApi.getPosts()
            .then(posts => {
                dispatch(fetchPostsSuccessAction(posts))
            })
            .catch(error => {
                dispatch(fetchPostsErrorAction(error.message))
            })
    }

    
}