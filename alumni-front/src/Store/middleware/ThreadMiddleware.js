import Post from "../../components/Post/Post"
import {PostApi} from "../../Services/APIService"
import {
    ACTION_COMMENT_CREATE,
    ACTION_ROOT_SET,
    commentErrorAction,
    commentSuccessAction,
    rootErrorAction,
    rootSuccessAction,

} from "../actions/ThredActions"

export const ThreadMiddleware = ({ dispatch }) => (next) => async (action) => {
    next(action)
    if (action.type === ACTION_ROOT_SET){
        PostApi.getPostByID(action.payload)
        .then(post => {
            dispatch(rootSuccessAction(post))
        })
        .catch(err =>{
            dispatch(rootErrorAction(err.message))
        })
    }

    if (action.type === ACTION_COMMENT_CREATE){
        PostApi.createComment(action.payload)
                .then(comment => {
                    dispatch(commentSuccessAction(comment)) 
                })
                .catch(err => {
                    dispatch(commentErrorAction(err.message))
                })

    }

    
}
