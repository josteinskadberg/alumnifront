import {EventApi} from "../../Services/APIService"
import {
    ACTION_EVENT_SET,
    eventErrorAction,
    eventSuccessAction,

} from "../actions/EventActions"

export const EventMiddleware = ({ dispatch }) => (next) => async (action) => {
    next(action)

    if (action.type === ACTION_EVENT_SET)
    EventApi.getEvents()
            .then(posts => {
                dispatch(eventSuccessAction(posts))
            })
            .catch(error => {
                dispatch(eventErrorAction(error.message))
            })
    }
    
