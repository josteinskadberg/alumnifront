export const ACTION_EVENT_SET = "[event] FETCH"
export const ACTION_EVENT_ERROR = "[event] ERROR"
export const ACTION_EVENT_SUCCESS = "[event] SUCCESS"


export const eventSetAction = (payload) => ({
    type: ACTION_EVENT_SET
})

export const eventSuccessAction = (success) => ({
    type : ACTION_EVENT_SUCCESS,
    payload: success
})

export const eventErrorAction = (error) => ({
    type :ACTION_EVENT_ERROR,
    payload: error  
})


