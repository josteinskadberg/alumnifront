export const ACTION_USER_SET = "[user] FETCH"
export const ACTION_USER_SUCCESS = "[user] SUCCESS"
export const ACTION_USER_ERROR = "[user] ERROR"
export const ACTION_OTHER_USER_SET = "[user] SET OTHER"
export const ACTION_USER_EDIT = "[user] EDIT"

export const userSetAction = () => ({
    type: ACTION_USER_SET
})

export const otherUserSetAction = (id) => ({
    type: ACTION_OTHER_USER_SET,
    payload: id 
})

export const userSuccessAction = (success) => ({
    type : ACTION_USER_SUCCESS,
    payload: success
})

export const userErrorAction = (error) => ({
    type :ACTION_USER_ERROR,
    payload: ACTION_USER_ERROR  
})

export const userEditAction = (user) =>({
    type : ACTION_USER_EDIT,
    payload : user
})


