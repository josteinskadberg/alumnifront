
export const ACTION_POST_FETCHALL = "[post] FETCH"
export const ACTION_POST_SUCCESS = "[post] SUCCESS"
export const ACTION_POST_ERROR = "[post] ERROR"

export const fetchPostsAction = () => {
    return {
        type: ACTION_POST_FETCHALL
    }
}
export const fetchPostsSuccessAction = posts => {
   
    return {
        type: ACTION_POST_SUCCESS,
        payload: posts
    }
}
export const fetchPostsErrorAction = error => {
    return {
        type: ACTION_POST_ERROR,
        payload: error
    }
}
