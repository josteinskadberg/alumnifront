export const ACTION_GROUPS_SET = "[group] FETCH"
export const ACTION_GROUP_SUCCESS = "[group] SUCCESS"
export const ACTION_GROUP_ERROR = "[group] ERROR"


export const groupsSetAction = () => ({
    type: ACTION_GROUPS_SET
})

export const groupSuccessAction = (success) => ({
    type : ACTION_GROUP_SUCCESS,
    payload: success
})

export const groupErrorAction = (error) => ({
    type :ACTION_GROUP_ERROR,
    payload: error  
})

 


