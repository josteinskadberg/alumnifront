export const ACTION_COMMENT_ERROR = "[thread] ERROR" 
export const ACTION_COMMENT_CREATE = "[thread] CREATE"
export const ACTION_ROOT_SET = "[thread] SET ROOT"
export const ACTION_ROOT_SUCCESS = "[thred] SUCCESS ROOT"
export const ACTION_ROOT_ERROR = "[thread] ROOT ERROR"
export const ACTION_COMMENT_SUCCESS = "[success]COMMENT SUCCESS"

export const commentErrorAction = error => {
    return {
        type: ACTION_COMMENT_ERROR,
        payload: error
    }
}

export const commentCreateAction = comment => {
    return {
        type: ACTION_COMMENT_CREATE,
        payload: comment
    }
}

export const rootSetAction = root => {
    return {
        type: ACTION_ROOT_SET,
        payload: root 
    }
}

export const rootSuccessAction = post => {
    return{
        type: ACTION_ROOT_SUCCESS, 
        payload : post
    }
}

export const rootErrorAction = error => {
    return{
        type: ACTION_ROOT_ERROR,
        payload: error
    }
}

export const commentSuccessAction = (newComment) => {
    return {
        type: ACTION_COMMENT_SUCCESS,
        payload: newComment
    }
}