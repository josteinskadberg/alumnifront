import { ACTION_EVENT_SET, ACTION_EVENT_SUCCESS, ACTION_EVENT_ERROR} from "../actions/EventActions"
 
const initialState = {
    loading: false,
    events: [],
    error: ""
}

export const EventReducer = (state = initialState, action) => {
    switch (action.type) {

        case ACTION_EVENT_SET:
            return {
                ...state,
                loading: true,
                error: ""
            }
        case ACTION_EVENT_SUCCESS:
            return {
                ...state,
                loading: false,
                events: action.payload
            }

        case ACTION_EVENT_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            }

        default:
            return state
    }
}