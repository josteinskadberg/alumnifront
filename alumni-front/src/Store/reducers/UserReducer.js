import {
ACTION_USER_SET,
ACTION_USER_ERROR,
ACTION_USER_SUCCESS,
ACTION_OTHER_USER_SET
} from "../actions/UserAction"

const initialState = {
    loading: false,
    user: {},
    error: ""
}


export const UserReducer = (state = initialState, action) => {
    switch (action.type) {

        case ACTION_USER_SET:
            return {
                ...state,
                loading: true,
                error: ""
            }
        case ACTION_OTHER_USER_SET:
            return {
                ...state,
                loading: true,
                error: ""
            }
        case ACTION_USER_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            }

        case ACTION_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                user: action.payload
            }

        default:
            return state
    }
}
