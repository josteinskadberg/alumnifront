import { ACTION_COMMENT_CREATE, ACTION_ROOT_SET, ACTION_COMMENT_ERROR, ACTION_ROOT_SUCCESS , ACTION_ROOT_ERROR, ACTION_COMMENT_SUCCESS} from "../actions/ThredActions"

const initialState = {
    loading: false,
    rootPost: {},
    commentError: "", 
    threadError: "",
    posted: {
        id: 0, 
        lastUpdated: "",
        text: "",
        user : {},
        comments: []
    }
}


export const ThreadReducer = (state = initialState, action) => {
    switch (action.type) {

        case ACTION_COMMENT_CREATE:
            return {
                ...state,
                loading: true,
                error: "" 
            }
        case ACTION_COMMENT_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            }

        case ACTION_ROOT_SUCCESS:
            return {
                ...state,
                loading: false,
                rootPost: action.payload
            }

        case ACTION_ROOT_SET:
            return {
                ...state, 
                loading: true,
                threadError: "", 
                commentError: ""
            } 
        case ACTION_ROOT_ERROR:
            return{
                ...state, 
                loading: false,
                threadError: action.payload
            }
        case ACTION_COMMENT_SUCCESS: 
            return{
                ...state,
                loading: false, 
                posted: action.payload
            }

        default:
            return state
    }
}