import { ACTION_GROUPS_SET, ACTION_GROUP_SUCCESS, ACTION_GROUP_ERROR} from "../actions/GroupActions"
 
const initialState = {
    loading: false,
    groups: [],
    error: ""
}

export const GroupReducer = (state = initialState, action) => {
    switch (action.type) {

        case ACTION_GROUPS_SET:
            return {
                ...state,
                loading: true,
                error: ""
            }
        case ACTION_GROUP_SUCCESS:
            return {
                ...state,
                loading: false,
                groups: action.payload
            }

        case ACTION_GROUP_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            }

        default:
            return state
    }
}