import { combineReducers } from "redux";
import { PostReducer } from "./PostReducer";
import {GroupReducer} from "./GroupReducer"; 
import {UserReducer} from "./UserReducer";
import {EventReducer} from "./EventReducer"; 
import {ThreadReducer} from "./ThreadReducer"

const appReducer = combineReducers({
    PostReducer,
    GroupReducer,
    UserReducer,
    EventReducer, 
    ThreadReducer
})

export default appReducer 

