import { ACTION_POST_SUCCESS,ACTION_POST_ERROR, ACTION_POST_FETCHALL } from "../actions/PostActions"

const initialState = {
    loading: false,
    posts: [],
    error: ""
}


export const PostReducer = (state = initialState, action) => {
    switch (action.type) {

        case ACTION_POST_FETCHALL:
            return {
                ...state,
                loading: true,
                error: ""
            }
        case ACTION_POST_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            }

        case ACTION_POST_SUCCESS:
            return {
                ...state,
                loading: false,
                posts: action.payload
            }

        default:
            return state
    }
}