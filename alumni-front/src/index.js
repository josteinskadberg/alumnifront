import React from 'react';
import ReactDOM from 'react-dom';
import "bootstrap/dist/css/bootstrap.min.css";
import './index.css';
import App from './App';
import {Provider} from "react-redux"
import Store from './Store';
import AuthService from './Services/AuthService';
import { divider } from '@uiw/react-md-editor';

function refreshPage() {
  window.location.reload()
}

const render = () =>  {ReactDOM.render(
  <React.StrictMode>
    <Provider store = {Store}>
    <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
}
const errorRender = () => {
  ReactDOM.render(
    <div className='text-center text-white h-100 row align-items-center'>
      <div className='col mt-xxl-5'>
      <h2>Server Error</h2>
      <h3>Authentication server is currently unavailable</h3>
      <h3>It can take a couple of minutes for the server to activate</h3>
      <h4>Please try to reaload the page</h4>
      <button className = "btn btn-warning" onClick = {refreshPage}>Reload Page</button>
      </div>
    </div>,document.getElementById('root'))
    
}



AuthService.initKeycloak(render)
