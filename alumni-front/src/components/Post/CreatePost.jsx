import React, {useState } from 'react';
import MDEditor from '@uiw/react-md-editor';
import AuthService from '../../Services/AuthService';
import Post from "./Post"
import  {PostApi} from "../../Services/APIService"
const CreatePost = (props) => {
    //const markDownMode = props.markDownMode 

    const [postMeta, setPostMeta] = useState(
        {
            username: AuthService.getUsername(),
            targetType: "",
            targetId: 0,
            title: "",
            comments: []
        });
    const [postText, setPostText] = useState("")

    const [markDownMode, setMarkdownMode] = useState(false)
    const [Preview, setPreview] = useState(false)

    const handleOnMarkdownModeClick = event => {
        event.preventDefault()
        setMarkdownMode(!markDownMode)
    }

    const handleOnPreviewClick = event => {
        event.preventDefault()
        setPreview(!Preview)
    }

    const onInputChange = event => {
        setPostMeta({
            ...postMeta,
            [event.target.id]: event.target.value
        })
    }

    const onSubmit = event => {
        event.preventDefault()
            
       const [response, error] = PostApi.CreatePost(
            {
                title: postMeta.title,
                text: postText,
                target: postMeta.targetType,
                sender: postMeta.sender
            })
    }

    const setTextFromTextArea = event => {
        setPostText(event.target.value)
    }
    return (

        <main className='container border-1'>

            <form>
                <input id="title" type="text" placeholder='enter a title' className='formControl' onChange={onInputChange} />

                {markDownMode
                    ?
                    <div className='mardown-mode'>
                        <MDEditor
                            value={postText}
                            onChange={setPostText}
                            id="text"
                        />
                        <button onClick={handleOnPreviewClick}>Preview post</button>
                    </div>
                    : <textarea className="w-100" id="text" rows="10" cols="50" onChange={setTextFromTextArea}>{postText}</textarea>
                }
                {Preview &&
                <Post {...postMeta} text = {postText}></Post>
                }

                <button className='btn text-primary' onClick={handleOnMarkdownModeClick}>Markdownmode</button>
                Post to
                <select value="Group">
                    <option value="Group">Group</option>
                    <option value="Topic">Topic</option>
                    <option value="User">User</option>
                </select>
                <select name="commnities" id="" value="select target"></select>

            </form>

            <button className="btn btn-primary btn-lg" onClick={onSubmit}>submit</button>
        </main>
    );
}



export default CreatePost;