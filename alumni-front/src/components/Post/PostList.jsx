import React, {useState} from 'react';
import Post from './Post';
import Thread from '../Thread/Thread';

function PostList({posts}) {
    const  [openPane, setOpenPane] = useState(false)
    const [targetPost, setTargetPostId] = useState({
        id: 0,
        user: {},
        lastUpdated: "",
        title: "",
        text: "",
        targetGroupId: 0,
        targetUSerId: 0,
        targetEventId: 0,
        targetTopicId: 0,
        comments: []
    })

    function openThred(open, postid) {
        setOpenPane(open)
        const target =  posts.find(post => post.id === postid)
        setTargetPostId(target)
    }


    return (
   
        <div>
            <div>
             <Thread post = {targetPost} isOpen = {openPane} reset = {() =>setOpenPane(false)}></Thread>
             </div>
             {posts.length ? (
                posts.map((post) => 
            <Post 
            rootId={post.id}
            post = {post}
            onClick = {openThred}
            key = {`${post.id}${new Date().getTime()}`}
             >     
            </Post>)
            )
            :
            <></>
            }
        </div>
    );
}

export default PostList;

