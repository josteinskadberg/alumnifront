import { group } from '@uiw/react-md-editor';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import MDEditor from '@uiw/react-md-editor';
import "./PostStyling.css"

function Comment(props) {
    const com = props["props"]

    return (
        <div className="card">
                <div className="card-header border rounded-top">
                    {com.username}
                    <span className='lastUpdated'> {com.lastUpdated}</span>  
                </div>
                <div className="card-body">
                    <div className="card-text">            
                        <MDEditor.Markdown
                            source={com.text} 
                        />
                    </div>
                        
                </div>
                <footer>
                    <button>{com.comments.length.toString() + " Comments"}</button>
                </footer>
            </div>

    );
}

export default Comment;
