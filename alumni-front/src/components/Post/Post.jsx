import React, { useEffect, useState } from 'react';
import { NavLink, Link, useSearchParams } from 'react-router-dom';
import AppContainer from '../../HOC/AppContainer';
import MDEditor from '@uiw/react-md-editor';
import "./PostStyling.css"
import Thread from '../Thread/Thread';
import { useDispatch, useSelector } from 'react-redux';
import { commentCreateAction } from '../../Store/actions/ThredActions';

const setPostStyle = (targetType) => {

    switch (targetType) {
        case ("event"): return "rgb(81, 129, 184)"
        case ("group"): return "rgb(42, 88, 133)"
        case ("topic"): return "rpg(255, 205, 36)"
        case ("inbox"): return "rgb(237, 238, 240)"
        default: return "rgb(237, 238, 240)"
    }

}

function Post({ post, onClick, threadMode, showComments, rootId }) {

    const getTargetType = (post) => {
        switch (true) {

            case (post.targetEventId != 0): return "event"

            case (post.targetGroupId != 0): return "group"

            case (post.targetTopicId != 0): return "topic"

            case (post.targetUserId != 0): return "inbox"

            default: return ""
        }

    }
    const targetType = getTargetType(post)
    const postStyle = { backgroundColor: setPostStyle(targetType) }
    const icon = "TopicIco"
    const to = `${targetType}/${post.target}`//link 
    const by = `/user/${post.user.username}/${post.user.id}` //link 
    const [showRepyInput, setShowReplyInput] = useState(false)
    const [show, setShow] = useState(false);
    const [postText, setPostText] = useState("")
    const { posted, loading, commentError } = useSelector((state => state.ThreadReducer))
    const [comments, setComments] = useState(post.comments)
    const [commentPosted, setCommentPosted] = useState(false)
    const dispatch = useDispatch()

    const openThred = (event) => {
        onClick(true, post.id)
    }

    useEffect(() => {

        if (commentPosted && Object.keys(posted).length > 0 && commentError === "" && !loading) {
            setComments(c => 
                [...comments, {...posted,comments : []}]
            )
            setCommentPosted(false)
        }
    }, [posted, commentPosted])

    const handlePublishReply = () => {
        
        if (postText.length > 0) {
            let parent = post.id 
            if (rootId === parent){
                parent = 0
            }
            const newcomment = {
                text: postText,
                postId: rootId,
                parentCommentId : parent,  
            }
            setShowReplyInput(false)
            setCommentPosted(true)
            dispatch(commentCreateAction(newcomment))
        }
    }

    return (
        <AppContainer>
            <div className="post-container card post">

                <div className="card-header border rounded-top" style={postStyle}>

                    <Link className='' to={by}> {post.user.username}</Link>
                    <span className='lastUpdated'> {post.lastUpdated}</span>
                </div>
                <div className=''>
                    <div className="card-body">
                        <h5 className="card-title" >{post.title}</h5>
                        <div className="card-text">
                            <MDEditor.Markdown
                                source={post.text}
                            />
                        </div>

                    </div>
                    <footer className='flex-row justify-content-between'>
                        {comments.length > 0 && threadMode &&
                                <button className="btn btn-light" onClick={() => setShow(!show)}>{comments.length.toString() + " Comments"}</button>
                               
                            
                        }
                        {threadMode &&  <button className='btn btn-light' onClick={() => setShowReplyInput(!showRepyInput)} >Reply</button>}
                        {!threadMode &&
                            <div>
                                <div>{comments.length.toString() + " Comments"}</div>
                                <button className="btn btn-light w-100" onClick={openThred}>View thread</button>
                            </div>
                        }
                    </footer>
                    {showRepyInput &&
                        <div className='clearfix'>
                            <MDEditor
                                value={postText}
                                onChange={setPostText}
                            />
                            <button onClick={handlePublishReply} className='float-end btn btn-primary '>Publish reply</button>
                            {commentError && <div className='alert alert-danger alert-dismissible fade show'>
                                <strong>Error!</strong> A problem has occurred while submitting your comment. </div>}
                        </div>


                    }

                    {comments.length > 0 && (show || showComments) &&
                        <div className='ms-lg-5'>
                            {comments.map((comment, index) =>
                                <Post threadMode={true} className post={comment} rootId={rootId}> </Post>
                            )}
                        </div>
                    }
                </div>
            </div>

        </AppContainer >
    );
}

export default Post;

