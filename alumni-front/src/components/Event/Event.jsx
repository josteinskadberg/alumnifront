import React from "react";
import { NavLink, Link } from "react-router-dom";
import AppContainer from "../../HOC/AppContainer";
import { useEffect, useState } from 'react';
import { groupAPI, UserAPI } from "../../Services/APIService";

function Event({createdBy, description, endTime, group, id, lastUpdated, name, rsvp, startTime, topics, users, objEvent, goingUrl})  {
    const to =`${goingUrl}/${id}`
    const [show, setShow] = useState(false);
    const [userCreate, setCreate] = useState();

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    // Get userdata from api
    const getUser = async() =>{
        try{
            const response = await UserAPI.getUserProfile(createdBy)
            setCreate(response.username)
        } catch(e){

        }
    }

    useEffect(() =>{
        getUser();
        

    }, [])
    
    return (
        
        <AppContainer>
            <div>
                
         <div className="card" style={{ width: '500px' }}  onClick={handleShow}>
            <div className="card-body">
            <NavLink to={to} style={{ textDecoration: 'none', color:"black"}}>
                <h4 className="card-title text-center">{name}</h4>
                </NavLink>
                <h5 className="card-subtitle mb-2 text-muted">Description</h5>
                <p class = "card-text">
                    {description}
                </p>
                <ul className="list-group list-group-flush">
                <li className="list-group-item">Start time: {startTime}</li>
                <li className="list-group-item">End time: {endTime}</li>
                <li className="list-group-item">Last updated: {lastUpdated}</li>
                
            </ul>
                <p className="card-text">Created by: {userCreate}</p>
            </div>
            </div>
            

            </div>
        </AppContainer>
    )
}

export default Event