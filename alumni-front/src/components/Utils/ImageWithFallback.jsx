import React, {useRef} from 'react';

function ImageWithFallback({src, className}) {
    const imgRef = useRef();
    const onImageError = () => imgRef.current.src="/blank-profile-picture.png";
    return (
      <img className = {className} ref={imgRef} src={src} onError={onImageError}/>
    )
}

export default ImageWithFallback;