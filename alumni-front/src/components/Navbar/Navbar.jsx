import React from "react";
import { Link } from "react-router-dom";
import "./Navbar.css"
import AuthService from "../../Services/AuthService";
const Navbar = () => {

    const handleOnLogoutClick = () =>{
        AuthService.doLogout()
    } 
    return(
        <nav className="navbar">
            <div className="navContainer">

            <ul>
                <Link to='timeline'>
                    <li>Timeline</li>
                </Link>
                <Link to='profile'>
                    <li>Profile</li>
                </Link>
                <Link to='topics'>
                    <li>Topics</li>
                </Link>
                <Link to='groups'>
                    <li>Groups</li>
                </Link>
                <Link to='calender'>
                    <li>Calendar</li>
                </Link>
                <Link to='events'>
                    <li>Events</li>
                </Link>
            </ul>
            {AuthService.isLoggedIn && <>
            <button className="btn btn-primary" onClick={handleOnLogoutClick}>Log out</button></>}
            </div>
        </nav>
    )
}

export default Navbar



