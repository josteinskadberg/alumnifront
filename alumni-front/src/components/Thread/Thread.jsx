import React, { Component, useEffect, useState } from "react";
import { render } from "react-dom";
import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";
import Post from "../Post/Post";

function Thread({ post, isOpen, reset}) {

    const [state, setState] = useState({
        isPaneOpen: false,
    })
        useEffect(() => {
            setState({isPaneOpen: isOpen})
        }, [isOpen,reset])
        
        return(
          <SlidingPane
            className="some-custom-class"
            overlayClassName="some-custom-overlay-class"
            isOpen={
                state.isPaneOpen
            }
            title="Hey, it is optional pane title.  I can be React component too."
            subtitle="Optional subtitle."
            onRequestClose={() => {
              // triggered on "<" on left top click or on outside click
              setState({ isPaneOpen: false });
              reset()
            }}
          >
            <Post post = {post} rootId = {post.id} showComments = {true} onReplyClick = {() => {}} threadMode = {true}></Post>
          </SlidingPane>
      );
};


export default Thread
