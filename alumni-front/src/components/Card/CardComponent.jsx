import React from 'react';
import { NavLink } from 'react-router-dom';
import AppContainer from '../../HOC/AppContainer';
import { Link } from "react-router-dom";

function CardComponent({createdBy, groupName, subscribed, isPrivate, description, groupId, goingUrl}) {
    const to = `${goingUrl}/${groupId}`

    const handleOnclickSubscribed = () =>{
       
    }


    return (
        <div className="card">
            <div className="card-header">
                {isPrivate
                ? <h5>Private</h5>
                : <h5>Public</h5>}
            </div>

  <div className="card-body">
    <NavLink to={to}> <h5 className="card-title">{groupName}</h5>
    </NavLink>
    <p className="card-text">{description}</p>
        <NavLink to="#">
            <button className='btn btn-primary'>
            {subscribed 
        ? "Unsubscribe"
        : "Subscribe"}
            </button>
    
    </NavLink>
  </div>
</div>


    );
}

export default CardComponent;

