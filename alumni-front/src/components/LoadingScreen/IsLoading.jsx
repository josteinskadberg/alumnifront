import React from "react";
import './IsLoading.css';
import Authenticated from "../../HOC/Authenticated";

function IsLoading() {
    return (
        <div className="loader"></div>
    )
}

export default Authenticated(IsLoading)