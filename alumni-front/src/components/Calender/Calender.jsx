import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import { format, parse, startOfWeek, getDay } from "date-fns";
import "react-big-calendar/lib/css/react-big-calendar.css";
import React, { useState } from "react";
import Authenticated from "../../HOC/Authenticated";
import { Navigate, useNavigate } from "react-router-dom";


function Calender(props) {

    const navigate = useNavigate();

    const calenderEvents = props.events.map((event) => {  
        return {
            title: event.name,
            start: new Date(event.startTime),
            end: new Date(event.endTime),
            id: event.id
        }
    })

    const handleOnClickEvent = (event) => {

         navigate(`/events/eventSingle/${event.id}`, { replace: true }) 

    }

    const locales = {
        "en-GB": require("date-fns/locale/en-GB")
    }
    
    const localizer = dateFnsLocalizer({
        format,
        parse,
        startOfWeek,
        getDay,
        locales
    })
    
    return(
        <div className="EventCalendar">
            <Calendar localizer={localizer} events={calenderEvents} onSelectEvent={handleOnClickEvent}
             startAccessor="start" endAccessor="end" 
             style={{height: 500, margin: "50px"}}/>
        </div>
    )
}

export default Authenticated(Calender)