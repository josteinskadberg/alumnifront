import React, { useEffect } from 'react';
import {useDispatch, useSelector} from "react-redux"
import AuthService from '../../Services/AuthService';
import { fetchPostsAction } from '../../Store/actions/PostActions';
import Authenticated from '../../HOC/Authenticated';
import PostList from '../../components/Post/PostList';
import Post from '../../components/Post/Post';
import IsLoading from '../../components/LoadingScreen/IsLoading';
function Timeline(props) {

    const dispatch = useDispatch()
    const {posts, loading, error} = useSelector((state) => state.PostReducer)

    useEffect(() => {
        dispatch(fetchPostsAction())
    },[])

    useEffect(() => {
        console.log(posts)
    },[posts])




    return (
        <>
        <div>
            <h1>Timeline</h1>
            {posts.length && <PostList posts = {posts}></PostList>}
         
        </div>
        <div>{error}</div>
        </>
    );

}

export default Authenticated(Timeline);