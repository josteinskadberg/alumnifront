import { set } from 'date-fns';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { EventApi, groupAPI, PostApi, UserAPI } from '../../Services/APIService';
import "./GroupStyling.css"
import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import Calender from '../../components/Calender/Calender';
import PostList from '../../components/Post/PostList';
import {useSelector} from "react-redux"
function GroupSingle(props) {


    const {groupId} = useParams();

    const [show, setShow] = useState(false);
    const [group, setGroup] = useState([])
    const [members, setMembers] = useState([])
    const [events, setEvents] = useState([])
    const [posts, setPosts] = useState([])
    const [select, setSelect] = useState([])
    const [selectedPost, setSelectedPost] = useState([])
    const {user} = useSelector((state)=> state.UserReducer)
    const isMember = () => user.groups.include(groupId)

    
    // Gets the group from api
    const getGroup = async() =>{
        
        try{
            const response = await groupAPI.getGroup(groupId)
            setGroup(response);
           // setMembers(response.users)

            return response;
        } catch (e){
            return e
        }
    }

    // Gets alle the events
    const getEvents = async(eventId) =>{
        try {
            const response = await EventApi.getEvent(eventId)
            let eventTemp = events
            eventTemp.push(response)
            setEvents(eventTemp)

            console.log(events)
        } catch(e){

        }
    }

    // Get the events
    // Gets the posts in the group
    const getPosts = async() =>{
        try{
            const response = await PostApi.getGroupPosts(groupId)
            setPosts(response);
            console.log(response);
        } catch(e){
            return e
        }
    }

    // Gets all the members
    const getMember = async(userId) =>{
        try{
            const response = await UserAPI.getUserProfile(userId)
            let memberTemp = members
            memberTemp.push(response)
            setMembers(memberTemp)

        } catch(e){

        }
    }

    // Join group
    const joinGroup = async() =>{
        console.log("asd")
        
        try {
            const response = await groupAPI.joinGroup(groupId)
            console.log(response)
                
        } catch(e){

        }
    }



    useEffect(() =>{
        
        if(group.length  ==0){
            getGroup();
            getPosts();
        }
        
        // Get events
        if(group.length != 0 && events.length == 0){

            group.events.forEach(eventId => {
                getEvents(eventId);
            });
            
        }

        // Get members
        if(group.length != 0 && members.length == 0){
            group.users.forEach(userId =>{
                getMember(userId)
            })
        }
        

    },[group]);

    return (
        <div className='container'>
            <h1 className='headingGroup'>{group.name}</h1>
            
            {group.isPrivate
            ? <p className='privateCheck rounded border '>Private</p>
            : <></>}

            <div className=' row'>
                <div className='col'>
                    <h3>Group Posts</h3>
                    {!isMember && <button className='btn btn-primary btn-md joinButton' onClick={joinGroup}>Join group</button>}
                    <div className='postList'>
                    <PostList posts = {posts}></PostList>
                </div>
              </div>
            
            
            <div className='col'>
            <div className='row'>
                    <h3>Description</h3>
                    <p>{group.description}</p>
                </div>
                <div id='groupList' className='row'>
                    <h3>Members</h3>
                    <ul className='list-group'>
                        
                            
                    {members.length ? (
                        members.map((member) =>
                        <li className='list-group-item border border-success' key = {member.id}>{member.username}</li>
                        
                    ))
                : <></>}
                    </ul>
                </div>
                :

                <div id='eventList' className='row'>

                </div>
                
            </div>
        </div>
        <Calender events={events}>

    </Calender>

    </div>
  );

}
export default GroupSingle;
