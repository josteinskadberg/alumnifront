import React from 'react';
import PropTypes from 'prop-types';
import CardComponent from '../../components/Card/CardComponent'
import { groupsSetAction } from "../../Store/actions/GroupActions"
import { useSelector, useDispatch } from "react-redux"
import { useEffect, useState } from 'react';
import { group } from '@uiw/react-md-editor';



function GroupList(props) {

    const dispatch = useDispatch()
    const {groups, loading, error } = useSelector((state) => state.GroupReducer)
    const [members, setMembers] = useState([])


    const renderGroups = () => {
        dispatch(dispatch(groupsSetAction()))

    }

    // Check if user is already member
    const checkIfMember = async () =>{
        

    }


    useEffect(() =>{
        dispatch(dispatch(groupsSetAction()))


    }, [])

    return (
        <>
        <div>
            {groups.length ? (
                groups.map((group) =>
                <CardComponent
                    goingUrl = "groupSingle"
                    groupId = {group.id}
                    key = {group.id}
                    description = {group.description}
                    createdBy={group.createdBy}
                    groupName ={group.name}
                    isPrivate = {group.isPrivate}
                    
                >
                </CardComponent>)
            )
            :<></>
            }
        </div>
        <div>{error}</div>
            </>
    );
}

export default GroupList;
