import Calender from "../../components/Calender/Calender";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { eventSetAction } from "../../Store/actions/EventActions";

function GroupCalendar() {

    const dispatch = useDispatch();
    const {events, loading, error} = useSelector((state) => state.EventReducer)

    useEffect(() => {
        dispatch(eventSetAction() [dispatch])
    })
    return (
        <Calender events={events}>

        </Calender>
    )
}

export default GroupCalendar