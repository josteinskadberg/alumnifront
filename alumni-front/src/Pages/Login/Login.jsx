import React from 'react';
import AuthService from '../../Services/AuthService';
import {Navigate} from "react-router-dom"
import { useDispatch } from 'react-redux';
import { userSetAction } from '../../Store/actions/UserAction';
import "./Login.css";
function Login(props) {

    const dispatch = useDispatch()
    
    if (AuthService.isLoggedIn()){
        dispatch(userSetAction())
        return <Navigate to = "/timeline"/>
    }

    const onHandleLoginClick = () => {
        AuthService.doLogin()
    }

    return (
        <>
        <h1>
            <span className="material-icons">&#xE87C;</span>
           Welcome to the Alumni Network 
        </h1>
        <div className='login-button'>
        <button onClick={onHandleLoginClick}>Join the network</button>
        </div>
        </>
    );
       
}

export default Login;