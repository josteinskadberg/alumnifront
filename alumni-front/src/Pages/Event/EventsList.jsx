import React from 'react';
import { useSelector, useDispatch } from "react-redux"
import { useEffect, useState } from 'react';
import { eventSetAction } from '../../Store/actions/EventActions';
import Event from '../../components/Event/Event';

function EventsList(props) {

    const dispatch = useDispatch()
    const {events, loading, error } = useSelector((state) => state.EventReducer)
    console.log(events)

    useEffect(() =>{
        dispatch(dispatch(eventSetAction()))
        

    }, [])

    return (
        <>
        {events.length ? (
            events.map((eventet) =>
            
            <Event
                goingUrl = "eventSingle"
                key = {eventet.id}
                createdBy = {eventet.createdBy}
                description = {eventet.description}

                endTime = {eventet.endTime}
                group = {eventet.group}
                id = {eventet.id}
                lastUpdated = {eventet.lastUpdated}
                name = {eventet.name}
                rsvp = {eventet.rsvp}
                startTime = {eventet.startTime}
                topics = {eventet.topics}
                users = {eventet.users}
                objEvent = {eventet}
                >
            </Event>)
        )
    :<></>}

        </>
    );
}

export default EventsList;
