import React from 'react';

import { useEffect, useState } from 'react';

import { useParams } from 'react-router-dom';
import { EventApi, groupAPI, PostApi, UserAPI } from '../../Services/APIService';

function EventInvite() {
    const {eventId} = useParams();
    const [inputValue, setInputValue] = useState(3);
    
    // Invite group 0 or user 1
    let inviteTarget = 0


    // Invite a group
    const groupInvite = async() =>{
        try{
            const response = await EventApi.createGroupInvitation(eventId, inputValue)
            console.log(response)
        } catch(e){
            console.log("Error")
        }
    }

    // Invite a user
    const userInvite = async() =>{
        try{
            const response = await EventApi.createUserInvitation(eventId, inputValue)
            console.log(response)
        } catch(e){
            console.log("Error")
        }
    }




    return (
        <>
                <select value="0" >
                    <option value="0">Group</option>
                    <option value="1">User</option>

                </select>
            <form >
            <label>
                Id: 
                 <input type="text" name="user"  placeholder='Identification'/>
            </label>

           
            
        </form>
        <button onClick={userInvite()}>Invite</button>
        </>

    );
}

export default EventInvite;
