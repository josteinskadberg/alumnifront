import React from 'react';

import { useEffect, useState } from 'react';

import { useParams } from 'react-router-dom';
import { EventApi, groupAPI, PostApi, UserAPI } from '../../Services/APIService';

function EventSingle() {
    const {eventId} = useParams();
    const [event, setEvent] = useState([0])
    const [posts, setPosts] = useState([])
    const [groups, setGroups] = useState([])
    const [users, setUsers] = useState([])
    const [creator, setCreator] = useState([0])

    // Gets the 5 first groups
    // index for group showing
    let showingGroup = 5;

    // Get the event from api
    const getEvent = async() =>{

        try{
            const response = await EventApi.getEvent(eventId)
            setEvent(response)
           // setPosts(response.posts)
           console.log(response)
           
        } catch(e){

        }
    }

    // Get user creator
    const getUserCreator = async() =>{
        try{
            
            const responseUser = await UserAPI.getUserProfile(event.createdBy)
            
            console.log(responseUser)
            setCreator(responseUser)
        } catch (e){

        }
    }

    // Get groups
    // Should add a limiter
    
    const getGroups = async(id) =>{
        console.log(id)
        const responeGroup = await groupAPI.getGroup(id)
        
       let groupsMidler = groups
       groupsMidler.push(responeGroup)

        setGroups(groupsMidler) 
        console.log(groups)
    }

    // Get users
    const getUsers = async(id) =>{
        console.log(id)
        const responseUserSingle = await UserAPI.getUserProfile(id)
        let userMidler = users
        userMidler.push(responseUserSingle)
        setUsers(userMidler)
        console.log(users)
    }


    // Get all posts
    const getPosts = async() =>{
        const responeEvent = await PostApi.getEventPosts(event.id)
        setPosts(responeEvent)
        console.log(posts)
    }

    useEffect(() =>{

        if(event == 0){
            getEvent();
        }
        
        // Gets the creator
        if(event != 0 && creator == 0){
            getUserCreator();
        }

        // Gets the posts
        if(event != 0 && posts.length == 0){
            getPosts();
        }

        // Gets the groups
        if(event != 0 && groups.length == 0){

            event.group.forEach(groupId => {
                getGroups(groupId);
            });

        // gets the users
        if(event != 0 && users.length == 0){

            event.users.forEach(userId => {
                getUsers(userId);
            })
        }
        }

    },[event, groups])

    return (
        <>
        <div className='container'>
            <div className=''>
                <div className='col'>

                </div>
                <h1>{event.name}</h1>
                <p>{event.description}</p>

                <div className='creatorTable col'>
                    

                    <h5 className ="card-title">Creator: {creator.username}</h5>
               
                </div>
                
                <div className='timeTable col'>
                <ul className='list-group'>
                    <li className='list-group-item'>Start time: {event.startTime}</li>
                    <li className='list-group-item'>End time: {event.endTime}</li>
                    <li className='list-group-item'>Last updated: {event.lastUpdated}</li>
                </ul>
                </div>
            </div>
            <div className=''>
                <div className='groupsTable col'>
                    <div className = "card" style={{ width: '18rem' }}>
                    <div className='card-body'>
                        <h4 className='card-title'>Groups</h4>
                        <ul className='list-group'>
                                {groups.length ? (
                                    groups.map((group) =>
                                        <li className='list-group-item' key={group.id}>
                                            <p>{group.name}</p>
                                            </li>
                                    )
                                ):
                                <></>}
                                </ul>
                    </div>
                    </div>
                </div>

                <div className='usersTable col'>
                    <div className='card' style={{ width: '18rem' }}>
                    <div className='card-body'>
                        <h4 className='card-title'>Users</h4>
                        <ul className='list-group'>
                                {users.length ? (
                                    users.map((user) =>
                                        <li className='list-group-item' key={user.id}>
                                            <p>{user.username}</p>
                                            </li>
                                    )
                                ):
                                <></>}
                                </ul>
                    </div>
                    </div>
                </div>



                
                <div className='postTable row'>
                    <div className='col'>
                        <h1>Posts</h1>
                        {posts.length ? (
                            posts.map((post) =>
                            <p>
                                {post.title}
                            </p>)
                        ): <></>}
                    </div>
                </div>
                
            </div>
            

            
        </div>
        </>
    );
}

export default EventSingle;
