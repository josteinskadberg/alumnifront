import React, { useEffect, useRef } from 'react';
import AppContainer from '../../HOC/AppContainer';
import "./Profile.css"
import { useNavigate, useParams } from "react-router-dom"
import { useDispatch, useSelector } from 'react-redux';
import Authenticated from '../../HOC/Authenticated';
import { otherUserSetAction, userSetAction } from '../../Store/actions/UserAction';
import ImageWithFallback from '../../components/Utils/ImageWithFallback';
import App from '../../App';
import IsLoading from '../../components/LoadingScreen/IsLoading';



function Profile(props) {

  const navigate = useNavigate()
  const dispatch = useDispatch()

  const urlParams = useParams()
  const { user, loading, error } = useSelector((state) => state.UserReducer)

  useEffect(() => {
    if (Object.keys(urlParams).length === 0) {
      dispatch(userSetAction())
    }
    else {
      const userId = parseInt(urlParams.userId)
      dispatch(otherUserSetAction(userId))
    }
  }, [urlParams])

  const handleOnSettingsClick = () => {
    navigate("settings")
  }

  return (
    <AppContainer>
      <div className='page-holder pt-4 px-4'>
        <div className="row">
          <div className="col-lg">

            <div className="card card-profile mb-3">

              <div className="card-header text-end">
                <button className='btn settings' onClick={handleOnSettingsClick}>
                  <span className='material-icons md-48 md-light'>settings</span>
                </button>
              </div>

              <div className="card-body text-center">
                <ImageWithFallback src={user.img} className="card-profile-img" />
                <h3 className="mb-3">{user.username}</h3>
                <p className="mb-4">{user.status}</p>
              </div>
            </div>

            <div className="card mb-4 text-center" >
              <div className="card-body">
                <div className="mb-3">
                  <p className="font-bold">Fun Fact About Me</p>
                  <p className="mb-4">{user.funFact}</p>
                  <label className="font-bold">Bio</label>
                  <p>{user.bio}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </AppContainer>
  );
}

export default Authenticated(Profile);