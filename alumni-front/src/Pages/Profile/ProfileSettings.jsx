import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import AppContainer from '../../HOC/AppContainer';
import { UserAPI } from '../../Services/APIService';
import { userEditAction, userSetAction } from "../../Store/actions/UserAction"

function ProfileSettings(props) {
  const dispatch = useDispatch()
  const { user, loading, error } = useSelector((state) => state.UserReducer)
  const [postStarted, setPostStarted] = useState(false)
  const navigate = useNavigate()

  const [updateUser, setUpdateUser] = useState({
    id: 0,
    img: "",
    status: "",
    bio: "",
    funFact: ""
  })

  useEffect(() => { dispatch(userSetAction()) }, [])
  useEffect(() => {
    setUpdateUser({
      id: user.id,
      img: user.img,
      status: user.status,
      bio: user.bio,
      funFact: user.funFact
    })
  }, [user])

  useEffect( () => {
    if(!loading && postStarted && !error){
      navigate("/profile")
    }
  },[loading, postStarted, error, navigate])


  const OnInputChange = event => {
    setUpdateUser({
      ...updateUser,
      [event.target.id]: event.target.value
    })
  }
  const handleOnClickSubmit = event => {
    event.preventDefault()
    setPostStarted(true)
    dispatch(userEditAction(updateUser))
    
  }
  return (
    <AppContainer>
        {error && <div className='alert alert-danger alert-dismissible fade show'>
          <strong>Error!</strong> A problem has occurred while submitting your data. </div>}
      <form className="card mb-4">
        <div className="card-body">
          <div className="row">
            <div className="col-sm-6 col-md-12">
              <div className="mb-4">
              </div>
              <div className="mb-4">
                <label
                  className="form-label"
                  aria-label="Profile Pic">
                  Profile Picture URL</label>
                <input
                  className="form-control"
                  type="text"
                  id="img"
                  placeholder="https://picture.com/picture"
                  onChange={OnInputChange}
                  value={updateUser.img}
                />
              </div>
            </div>
            <div className="col-sm-6 col-md-12">
              <div className="mb-4">
                <label
                  className="form-label"
                  aria-label="Work Status"
                >My Work Status</label
                >
                <input
                  className="form-control"
                  type="text"
                  id="status"
                  placeholder="Dream possition in dream company"
                  onChange={OnInputChange}
                  value={updateUser.status}
                />
              </div>
            </div>
            <div className="col-md-12">
              <div className="mb-4">
                <label
                  className="form-label"
                  aria-label="Fun Fact"
                >Fun Fact About Me</label
                >
                <input
                  className="form-control"
                  type="text"
                  id="funFact"
                  placeholder="I can roll my tongue into a flower"
                  onChange={OnInputChange}
                  value={updateUser.funFact}
                />
              </div>
            </div>
            <div className="col-md-12">
              <div className="mb-0">
                <label className="form-label" aria-label="Bio"
                >My Bio</label
                >
                <textarea
                  id="bio"
                  className="form-control"
                  rows="5"
                  placeholder="I grew up in Norway among a tribe of polarbears"
                  onChange={OnInputChange}
                  value={updateUser.bio}
                ></textarea>
              </div>
            </div>
          </div>
        </div>
        <div className="card-footer text-end">
          <button
            className="btn btn-primary"
            type="button"
            onClick={handleOnClickSubmit}
          >
            Update Profile
          </button>
        </div>
      </form>
    </AppContainer>


  );
}

export default ProfileSettings;