import axios from 'axios'
import AuthService from './AuthService'

const API_URL = 'https://alumniapi.azurewebsites.net/api'
export const PostApi = {

    async getPosts(searchParams = {}) {
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() } }
        const url = API_URL + "/Post"
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }

            })
        return response.data
    },

    async getPostByID(id){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() } }
        const url = API_URL + `/Post/${id}`
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }
            })
        return response.data
    },
    

    async getGroupPosts(id) {
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }}
        const url = API_URL + `/Post/group/${id}`
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }
            })
        return response.data
    },


    async getDirectMessages(id, searchParams = {}) {
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }, data: searchParams }
        const url = API_URL + `/Post/user/${id}`
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }
            })
        return response.data
    },

    async getEventPosts(id, searchParams = {}) {
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }, data: searchParams }
        const url = API_URL + `/Post/event/${id}`
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }
            })
        return response.data
    },


    async createPost(post) {
        const header = { headers: { Authorization: "Bearer " + AuthService.getToken() }}
        const url = API_URL + "/Post"
        const response = await axios.post(url, header)
            .catch((err) => {
                if (err.response) {
                    return [null, err.response]
                }
            })
        return [response.data, null]
    },


    async createComment(comment){
        const header = { headers: { Authorization: "Bearer " + AuthService.getToken()}}
        const url = API_URL + "/Post/Comment"
        const response = await axios.post(url,comment, header)
            .catch((err) => {
                if (err.response) {
                    throw(err)
                }
            })
        return response.data 
    }

}


export const groupAPI = {

    async getGroups(searchParams = {}) {
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }, data: searchParams }
        const url = API_URL + "/Group"
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }

            })
        console.log(response.data)
        return response.data
    },

    async getGroup(id) {
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }}
        const url = API_URL + `/Group/${id}`
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }

            })
        return response.data
    },

    async joinGroup(groupId){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken()}}
        const url = API_URL + `/group/${groupId}/join/userId`
        const response = await axios.post(url, {}, config)
            .catch((err) => {
                if (err.response) {
                    return [null, err.response]
                }
            })
            
            console.log(response.data)
        return [response.data, null]
    }
}


export const UserAPI = {

    async getUser(){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() } }
        const url = API_URL + "/User"
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }

            })
        return response.data
    },

    async getUserProfile(id){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() } }
        const url = API_URL + `/User/${id}`
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }

            })
            
        return response.data
    },

    async editUserProfile(user){
        const config = { headers:{ Authorization: "Bearer " + AuthService.getToken() }}
        const url = API_URL + `/User/${user.id}`
        console.log(url)
        const response = await axios.put(url, user, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }
            })
        return response.data
    }

} 


export const EventApi = {

    async getEvents(searchParams = {}){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() },data:searchParams }
        const url = API_URL + "/Event"
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }

            })
            return response.data
    },

    async getEvent(event){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() },data:event }
        const url = API_URL + `/Event/${event}`
        const response = await axios.get(url, config)
            .catch((err) => {
                if (err.response) {
                    throw Error(err)
                }

            })
            return response.data
    },

    async createEvent(event){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() } ,data: event}
        const url = API_URL + "/Event"
        const response = await axios.post(url, config) 
            .catch((err) => {
                if (err.response) {
                    return [null, err.response]
                }
            })
        return [response.data, null]
    },

    async editEvent(event){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }, data: event}
        const url = API_URL - `/Event/${event.id}`
        const response = await axios.put(url, config)
        .catch((err) => {
            if (err.response) {
                return [null, err.response]
            }
        })
        return [response.data, null]
    },


    async createGroupInvitation(eventId, groupId){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }}
        const url = API_URL + `/Event/${eventId}/invite/group/${groupId}`
        const response = await axios.post(url, config) 
            .catch((err) => {
                if (err.response) {
                    return [null, err.response]
                }
            })
        return [response.data, null]
    },
    async deleteGroupInvitation(eventId, groupId){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }}
        const url = API_URL + `/Event/${eventId}/invite/group/${groupId}`
        const response = await axios.post(url, config) 
            .catch((err) => {
                if (err.response) {
                    return [null, err.response]
                }
            })
        return [response.data, null]
    },

    async createTopicInvitation(eventId, topicId){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }}
        const url = API_URL + `/Event/${eventId}/invite/topic/${topicId}`
        const response = await axios.post(url, config) 
            .catch((err) => {
                if (err.response) {
                    return [null, err.response]
                }
            })
        return [response.data, null]
    },
    async deleteTopicInvitation(eventId, topicId){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }}
        const url = API_URL + `/Event/${eventId}/invite/topic/${topicId}`
        const response = await axios.delete(url, config) 
            .catch((err) => {
                if (err.response) {
                    return [null, err.response]
                }
            })
        return [response.data, null]
    },

    async createUserInvitation(eventId, userId){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }}
        const url = API_URL + `/Event/${eventId}/invite/user/${userId}`
        const response = await axios.post(url, config) 
            .catch((err) => {
                if (err.response) {
                    return [null, err.response]
                }
            })
        return [response.data, null]
    },

    async deleteUserInvitation(eventId, userId){
        const config = { headers: { Authorization: "Bearer " + AuthService.getToken() }}
        const url = API_URL + `/Event/${eventId}/invite/user/${userId}`
        const response = await axios.delete(url, config) 
            .catch((err) => {
                if (err.response) {
                    return [null, err.response]
                }
            })
        return [response.data, null]
    }
}